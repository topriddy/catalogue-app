Requirements
===============================
- requires JDK 8 or above
- requires Maven 3 or above
Running
=================================
From the root folder of the application
1. build the application running below 
    mvn package
2. run the application 
    java -jar target/catalogue-app-1.0-SNAPSHOT.jar
3. Access the application from the default port 8380 as below:
http://localhost:8380

Test Data
=================
Application can be run passing customerId to view product catalogue. Default is 1
    http://localhost:8380/
Passing arguments
------------------
- customerId: 1 (which is located in London)    : http://localhost:8380/customer/1
- customerId: 2 (which is located in Liverpool) : http://localhost:8380/customer/2  