'use strict';

/* App Module*/

var app = angular.module('catalogueApp', [
    'ngRoute',
    'catalogue.controllers',
    'catalogue.services'
]);

app.filter('categoryFilter', function () {
    return function (products, category) {
        var filtered = [];
        angular.forEach(products, function (product) {
            console.log(product.category.name);
            if (product.category.name === category) {
                filtered.push(product);
            }
        });
        return filtered;
    };
});

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider


        .when('/', {
            redirectTo: '/customer/1'
        })
        //route for the product selection page passing customerId
        .when('/customer/:customerId', {
            templateUrl: 'partials/catalogue.html',
            controller: 'CatalogueController',
        })
        //route for the product selection confirmation page
        .when('/confirm', {
            templateUrl: 'partials/confirmation.html',
            controller: 'ConfirmationController'
        })
        .otherwise({
            redirectTo: '/customer/1'
        });
}]);