'use strict';

/* Services */

var services = angular.module('catalogue.services',
    ['ngResource']);

services.factory('CustomerLocationService', ['$resource', function ($resource) {
    var url = "/rest/customerLocation/";

    return $resource(url, {});
}]);

services.factory('CatalogueService', ['$resource', function ($resource) {
    var basket = [];

    var url = "/rest/catalogue/products/";

    return $resource(url, {});
}]);




