'use strict';

/* Controllers */

var controllers = angular.module('catalogue.controllers', ['ngRoute']);

controllers.controller('CatalogueController', ['$scope', '$location', '$routeParams',
    'CustomerLocationService', 'CatalogueService', function ($scope, $location, $routeParams, CustomerLocationService, CatalogueService) {

        //simulates cookies by setting customerId from path.
        //defaults to 1
        var customerId = $routeParams.customerId || 1;

        CustomerLocationService.get({customerId: customerId}).$promise.then(function (data) {
            $scope.location = data;

            CatalogueService.query({locationId: $scope.location.id}).$promise.then(function (data) {
                $scope.products = data;
            });
        });

        $scope.updateBasket = function () {
            $scope.basket = [];

            angular.forEach($scope.products, function (product) {
                if (product.selected) {
                    $scope.basket.push(product);
                }
            });
        };

        $scope.checkOut = function () {
            CatalogueService.basket = $scope.basket;
            CatalogueService.basket.customerID = customerId;
            CatalogueService.basket.location = $scope.location;

            $location.path('/confirm');
        };
    }]);


controllers.controller('ConfirmationController', ['$scope', 'CatalogueService', function ($scope, CatalogueService) {
    $scope.basket = CatalogueService.basket;
}]);