package com.sky.topriddy.catalogue.core;

import lombok.Value;

import java.io.Serializable;

@Value
public class Product implements Serializable{
    private Long id;
    private String name;
    private ProductCategory category;
    private Location location;
}
