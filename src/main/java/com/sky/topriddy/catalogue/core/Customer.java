package com.sky.topriddy.catalogue.core;


import lombok.Value;

import java.io.Serializable;

/**
 * Customer representation
 */
@Value
public class Customer implements Serializable{
    private Long id;
    private String name;
}
