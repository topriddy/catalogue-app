package com.sky.topriddy.catalogue.core;

import lombok.Value;

import java.io.Serializable;

@Value
public class Location implements Serializable{
    private Long id;
    private String name;
}
