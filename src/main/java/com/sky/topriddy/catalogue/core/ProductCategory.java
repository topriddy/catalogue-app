package com.sky.topriddy.catalogue.core;

import lombok.Value;

import java.io.Serializable;

@Value
public class ProductCategory implements Serializable{
    private Long id;
    private  String name;
}
