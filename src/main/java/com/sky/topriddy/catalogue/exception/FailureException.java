package com.sky.topriddy.catalogue.exception;

public class FailureException extends Exception{
    public FailureException(String message){
        super(message);
    }
}
