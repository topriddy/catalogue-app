package com.sky.topriddy.catalogue.dao;

import com.sky.topriddy.catalogue.core.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {
    private final DataProviderStub dataProviderStub;

    public ProductDAOImpl(DataProviderStub dataProviderStub) {
        this.dataProviderStub = dataProviderStub;
    }

    @Override
    public List<Product> getProductByLocationID(Long locationID) {
        List<Product> products = new ArrayList<>();

        //returns empty product if location doesn't exist
        if (locationID == null || !locationIDExist(locationID))
            return products;

        for (Product product : dataProviderStub.getProducts().values()) {
            if (product.getLocation() == null) {
                //products without location are added to all customers
                products.add(product);
            } else if (product.getLocation().getId() == locationID) {
                products.add(product);
            }
        }
        return products;
    }

    private boolean locationIDExist(Long locationId) {
        return dataProviderStub.getLocations().containsKey(locationId);
    }
}

