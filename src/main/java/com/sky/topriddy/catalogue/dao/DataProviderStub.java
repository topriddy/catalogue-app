package com.sky.topriddy.catalogue.dao;


import com.sky.topriddy.catalogue.core.Customer;
import com.sky.topriddy.catalogue.core.Location;
import com.sky.topriddy.catalogue.core.Product;
import com.sky.topriddy.catalogue.core.ProductCategory;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DataProviderStub implements Serializable {


    @Getter
    private Map<Long, Location> locations;

    @Getter
    private Map<Long, Customer> customers;


    @Getter
    private Map<Long, ProductCategory> productCategories;

    @Getter
    private Map<Long, Product> products;


    public DataProviderStub() {
        buildData();
    }

    private void buildData() {
        locations = new HashMap<>();
        final Location londonLocation = new Location(1L, "London");
        final Location liverpoolLocation = new Location(2L, "Liverpool");
        locations.put(londonLocation.getId(), londonLocation);
        locations.put(liverpoolLocation.getId(), liverpoolLocation);

        customers = new HashMap<>();
        final Customer customer1 = new Customer(1L, "Jane Doe");
        final Customer customer2 = new Customer(2L, "John Foo");
        customers.put(customer1.getId(), customer1);
        customers.put(customer2.getId(), customer2);

        productCategories = new HashMap<>();
        final ProductCategory sportsCategory = new ProductCategory(1L, "Sports");
        final ProductCategory newsCategory = new ProductCategory(2L, "News");
        productCategories.put(sportsCategory.getId(), sportsCategory);
        productCategories.put(newsCategory.getId(), sportsCategory);

        products = new HashMap<>();
        products.put(1L, new Product(1L, "Arsenal TV", sportsCategory, londonLocation));
        products.put(2L, new Product(2L, "Chelsea TV", sportsCategory, londonLocation));
        products.put(3L, new Product(3L, "Liverpool TV", sportsCategory, liverpoolLocation));
        products.put(4L, new Product(4L, "Sky News", newsCategory, null));
        products.put(5L, new Product(4L, "Sky Sport News", newsCategory, null));
    }
}
