package com.sky.topriddy.catalogue.dao;

import com.sky.topriddy.catalogue.core.Product;

import java.util.List;

public interface ProductDAO {
    List<Product> getProductByLocationID(Long locationID);
}
