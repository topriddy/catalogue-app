package com.sky.topriddy.catalogue;


import com.sky.topriddy.catalogue.dao.DataProviderStub;
import com.sky.topriddy.catalogue.dao.ProductDAO;
import com.sky.topriddy.catalogue.dao.ProductDAOImpl;
import com.sky.topriddy.catalogue.resources.CatalogueResource;
import com.sky.topriddy.catalogue.resources.CustomerLocationResource;
import com.sky.topriddy.catalogue.service.CatalogueService;
import com.sky.topriddy.catalogue.service.CatalogueServiceImpl;
import com.sky.topriddy.catalogue.service.CustomerLocationService;
import com.sky.topriddy.catalogue.service.CustomerLocationServiceStub;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogueApp extends Application<CatalogueAppConfiguration> {
    private  static final Logger log = LoggerFactory.getLogger(CatalogueApp.class);


    public String getName(){
        return "catalogue-app";
    }

    @Override
    public void initialize(Bootstrap<CatalogueAppConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
    }

    @Override
    public void run(CatalogueAppConfiguration catalogueAppConfiguration, Environment environment) throws Exception {


        //create daos and services
        final DataProviderStub dataProviderStub = new DataProviderStub();
        final CustomerLocationService customerLocationService = new CustomerLocationServiceStub(dataProviderStub);
        final ProductDAO productDAO = new ProductDAOImpl(dataProviderStub);
        final CatalogueService catalogueService = new CatalogueServiceImpl(productDAO);

        //register the rest resources
        final CustomerLocationResource customerLocationResource = new CustomerLocationResource(customerLocationService);
        final CatalogueResource catalogueResource = new CatalogueResource(catalogueService);

        environment.jersey().register(customerLocationResource);
        environment.jersey().register(catalogueResource);
    }

    public static void main(String args[]) throws Exception {
        new CatalogueApp().run("server", "catalogue-app.yml");
    }
}
