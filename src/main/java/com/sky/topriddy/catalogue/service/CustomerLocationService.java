package com.sky.topriddy.catalogue.service;

import com.sky.topriddy.catalogue.core.Location;
import com.sky.topriddy.catalogue.exception.FailureException;

public interface CustomerLocationService {
    /**
     *
     * @param customerId
     * @return Location of a Customer
     * @throws FailureException if customerId doesn't exist
     */
    Location getLocation(Long customerId) throws FailureException;
}
