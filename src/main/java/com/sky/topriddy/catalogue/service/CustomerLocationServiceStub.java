package com.sky.topriddy.catalogue.service;

import com.sky.topriddy.catalogue.core.Location;
import com.sky.topriddy.catalogue.dao.DataProviderStub;
import com.sky.topriddy.catalogue.exception.FailureException;

public class CustomerLocationServiceStub implements CustomerLocationService {
    private final DataProviderStub dataProviderStub;
    private final Long LONDON_LOCATION_ID = 1L;
    private final Long LIVERPOOL_LOCATION_ID = 2L;

    public CustomerLocationServiceStub(DataProviderStub dataProviderStub){
        this.dataProviderStub = dataProviderStub;
    }

    @Override
    public Location getLocation(Long customerId) throws FailureException{
        Location location = null;
        try{
            if(customerId == 1L){
                location = dataProviderStub.getLocations().get(LONDON_LOCATION_ID);
            }else if(customerId == 2L){
                location = dataProviderStub.getLocations().get(LIVERPOOL_LOCATION_ID);
            }else{
                throw new FailureException("There was a problem retrieving the customer information");
            }
        }catch (Exception ex){
            throw new FailureException("There was a problem retrieving the customer information");
        }
        return location;
    }
}
