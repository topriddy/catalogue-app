package com.sky.topriddy.catalogue.service;

import com.sky.topriddy.catalogue.core.Product;

import java.util.List;

public interface CatalogueService {
    /**
     *
     * @param locationID Identifier of Location
     * @return a list of Product if locationID exist and an empty List otherwise
     *
     */
    List<Product> getProducts(Long locationID);
}
