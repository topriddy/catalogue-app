package com.sky.topriddy.catalogue.service;

import com.sky.topriddy.catalogue.core.Product;
import com.sky.topriddy.catalogue.dao.ProductDAO;

import java.util.List;

public class CatalogueServiceImpl implements CatalogueService{
    private final ProductDAO productDAO;

    public CatalogueServiceImpl(ProductDAO productDAO){
        this.productDAO = productDAO;
    }
    @Override
    public List<Product> getProducts(Long locationID) {
        return productDAO.getProductByLocationID(locationID);
    }
}
