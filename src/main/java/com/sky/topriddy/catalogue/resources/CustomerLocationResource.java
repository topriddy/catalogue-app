package com.sky.topriddy.catalogue.resources;

import com.sky.topriddy.catalogue.core.Location;
import com.sky.topriddy.catalogue.exception.FailureException;
import com.sky.topriddy.catalogue.service.CustomerLocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/customerLocation")
public class CustomerLocationResource {
    private static Logger log = LoggerFactory.getLogger(CustomerLocationResource.class);

    private final CustomerLocationService customerLocationService;

    public CustomerLocationResource(CustomerLocationService customerLocationService){
        this.customerLocationService = customerLocationService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerLocation(@QueryParam("customerId") Long customerId){
        try {
            Location location = customerLocationService.getLocation(customerId);
            return Response.ok(location).build();
        }catch(FailureException ex){
            log.error("", ex);
            throw new WebApplicationException(ex.getMessage(), Response.Status.NOT_FOUND);
        }
    }
}
