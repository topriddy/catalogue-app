package com.sky.topriddy.catalogue.resources;

import com.sky.topriddy.catalogue.core.Product;
import com.sky.topriddy.catalogue.service.CatalogueService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/catalogue")
public class CatalogueResource {
    private final CatalogueService catalogueService;

    public CatalogueResource(CatalogueService catalogueService){
        this.catalogueService = catalogueService;
    }

    @Path("/products")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProducts(@QueryParam("locationId") Long locationId){
        List<Product> products = catalogueService.getProducts(locationId);
        return Response.ok(products).build();
    }
}
