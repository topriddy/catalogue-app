package com.sky.topriddy.catalogue.dao;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DataProviderStubTest {

    private DataProviderStub dataProviderStub;

    @Before
    public void setUp(){
        dataProviderStub = new DataProviderStub();
    }

    @Test
    public void shouldInitializeDataProvider(){
        assertThat(dataProviderStub.getLocations()).isNotNull();
        assertThat(dataProviderStub.getLocations()).hasSize(2);

        assertThat(dataProviderStub.getCustomers()).isNotNull();
        assertThat(dataProviderStub.getCustomers()).hasSize(2);

        assertThat(dataProviderStub.getProductCategories()).isNotNull();
        assertThat(dataProviderStub.getProductCategories()).hasSize(2);

        assertThat(dataProviderStub.getProducts()).isNotNull();
        assertThat(dataProviderStub.getProducts()).hasSize(5);
    }
}