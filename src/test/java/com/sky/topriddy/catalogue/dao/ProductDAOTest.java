package com.sky.topriddy.catalogue.dao;


import com.sky.topriddy.catalogue.core.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductDAOTest {
    private ProductDAO productDAO;

    @Before
    public void setUp() {
        DataProviderStub dataProviderStub = new DataProviderStub();
        productDAO = new ProductDAOImpl(dataProviderStub);
    }

    @Test
    public void shouldGetProductGivenValidLocationID() {
        List<Product> products = productDAO.getProductByLocationID(1L);
        assertThat(products).isNotNull();
        assertThat(products).isNotEmpty();
    }

    @Test
    public void shouldGetNoProductsGivenNullLocationId(){
        List<Product> products = productDAO.getProductByLocationID(null);
        assertThat(products).isNotNull();
        assertThat(products).hasSize(0);
    }

    @Test
    public void shouldGetNoProductsGivenInvalidLocationId(){
        List<Product> products = productDAO.getProductByLocationID(Long.MAX_VALUE);
        assertThat(products).isNotNull();
        assertThat(products).hasSize(0);
    }
}
