package com.sky.topriddy.catalogue;


import com.sky.topriddy.catalogue.resources.CatalogueResource;
import com.sky.topriddy.catalogue.resources.CustomerLocationResource;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Environment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CatalogueAppTest {

    @Mock
    private Environment environment;

    @Mock
    private JerseyEnvironment jersey;

    private CatalogueApp application;
    private CatalogueAppConfiguration catalogueAppConfiguration;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        application = new CatalogueApp();
        catalogueAppConfiguration = new CatalogueAppConfiguration();
    }

    @Test
    public void shouldRegisterAllResource()throws Exception{
        when(environment.jersey()).thenReturn(jersey);

        application.run(catalogueAppConfiguration, environment);

        verify(jersey).register(isA(CustomerLocationResource.class));
        verify(jersey).register(isA(CatalogueResource.class));
    }

}