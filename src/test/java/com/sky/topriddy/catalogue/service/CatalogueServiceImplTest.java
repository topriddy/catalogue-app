package com.sky.topriddy.catalogue.service;

import com.sky.topriddy.catalogue.core.Product;
import com.sky.topriddy.catalogue.dao.DataProviderStub;
import com.sky.topriddy.catalogue.dao.ProductDAO;
import com.sky.topriddy.catalogue.dao.ProductDAOImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CatalogueServiceImplTest {
    private ProductDAO productDAO;

    private CatalogueService catalogueService;

    private final Long LONDON_LOCATION_ID = 1L;
    private final Long LIVERPOOL_LOCATION_ID = 2L;


    @Before
    public void setUp(){
        productDAO = new ProductDAOImpl(new DataProviderStub());
        catalogueService = new CatalogueServiceImpl(productDAO);
    }

    @Test
    public void shouldGetLondonProducts(){
        List<Product> productList = catalogueService.getProducts(LONDON_LOCATION_ID);
        assertThat(productList).isNotNull();
        assertThat(productList).hasSize(4);
        assertThat(productList).extracting("name")
                .containsExactly("Arsenal TV", "Chelsea TV", "Sky News", "Sky Sport News");
    }

    @Test
    public void shouldGetLiverpoolProducts(){
        List<Product> productList = catalogueService.getProducts(LIVERPOOL_LOCATION_ID);
        assertThat(productList).isNotNull();
        assertThat(productList).hasSize(3);
        assertThat(productList).extracting("name")
                .containsExactly("Liverpool TV", "Sky News", "Sky Sport News");
    }



}
