package com.sky.topriddy.catalogue.service;


import com.sky.topriddy.catalogue.core.Location;
import com.sky.topriddy.catalogue.dao.DataProviderStub;
import com.sky.topriddy.catalogue.exception.FailureException;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerLocationServiceStubTest {
    private CustomerLocationService customerLocationService;

    @Before
    public void setUp() {
        DataProviderStub dataProviderStub = new DataProviderStub();
        customerLocationService = new CustomerLocationServiceStub(dataProviderStub);
    }

    @Test
    public void shouldReturnLondonLocationGivenCustomerWithID1() throws FailureException {

        Location location = customerLocationService.getLocation(1L);
        assertThat(location).isNotNull();
        assertThat(location.getId()).isEqualTo(1L);
        assertThat(location.getName()).isEqualTo("London");
    }

    @Test
    public void shouldReturnLiverpoolLocationGivenCustomerWithID2() throws FailureException {

        Location location = customerLocationService.getLocation(2L);
        assertThat(location).isNotNull();
        assertThat(location.getId()).isEqualTo(2L);
        assertThat(location.getName()).isEqualTo("Liverpool");
    }

    @Test(expected = FailureException.class)
    public void shouldThrowFailureExceptionGivenNullCustomerID() throws FailureException {
        Location location = customerLocationService.getLocation(null);
    }

    @Test(expected = FailureException.class)
    public void shouldThrowFailureExceptionGivenNonExistCustomerID() throws FailureException {
        Location location = customerLocationService.getLocation(455L);
    }
}